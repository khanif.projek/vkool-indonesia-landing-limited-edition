<footer class="relative bg-black pt-8 pb-6">
  <div class="container mx-auto px-4">
    <div class="flex flex-wrap items-center md:justify-between justify-center">
      <div class="w-full md:w-4/12 px-4 mx-auto text-center">
        <div class="text-sm text-gray-600 font-semibold py-1">
          Copyright © {{ date('Y') }}
          <a href="https://www.vkool-indonesia.com" class="text-gray-600 hover:text-gray-900">V-Kool Indonesia</a>.
        </div>
      </div>
    </div>
  </div>
</footer>