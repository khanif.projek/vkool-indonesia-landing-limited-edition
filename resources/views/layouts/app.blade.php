<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="theme-color" content="#000000" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') &mdash; {{ config('app.name') }}</title>

    <meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
    <link rel="canonical" href="{{ url('/') }}" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="og:title"
        content="@yield('title') - {{ config('app.name') }}" />
    <meta property="og:description" content="" />
    <meta property="og:url" content="{{ config('app.url') }}" />
    <meta property="og:site_name"
        content="{{ config('app.name') }}" />

    <link rel="shortcut icon" href="{{ url('/assets') }}/img/favicon/favicon.ico" />
    <link rel="apple-touch-icon" sizes="180x180" href="{{ url('/assets') }}/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ url('/assets') }}/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ url('/assets') }}/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="{{ url('/assets') }}/img/favicon/site.webmanifest">

    <link
      rel="stylesheet"
      href="{{ url('/assets') }}/vendor/@fortawesome/fontawesome-free/css/all.min.css"
    />
    <link href="{{ url('/assets') }}/css/tailwind.min.css" rel="stylesheet">
    <!-- <link href="{{ url('/assets') }}/font/fonts.css" rel="stylesheet"> -->
    <link href="{{ url('/assets') }}/css/customs.css" rel="stylesheet">

    @stack('styles')

    {{-- Scripts --}}
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window,document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '717618115835615'); 
      fbq('track', 'PageView');
      </script>
      <noscript>
      <img height="1" width="1" 
      src="https://www.facebook.com/tr?id=717618115835615&ev=PageView
      &noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-PX5SS97');</script>
    <!-- End Google Tag Manager -->

</head>
<body class="text-gray-800 antialiased font-oswald">
  <main>
    @yield('content')
  </main>
  @include('layouts.footer')
    
    @stack('scripts')
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PX5SS97"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
</body>

</html>
