<div class="container mx-auto px-4 pt-20">
  <div class="flex flex-wrap justify-center">
    <div class="w-full lg:w-6/12 px-4">
      <div class="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-gray-300">
        <div class="flex-auto p-5 lg:p-10">
          <h4 class="text-2xl font-semibold">Ingin penawaran khusus?</h4>
          <p class="leading-relaxed mt-1 mb-4 text-gray-600">
            Silahkan daftar untuk diskon dan penawaran khusus
          </p>
          <form action="{{url('submit')}}" method="POST">
          {!! csrf_field() !!}
            <div class="relative w-full mb-3 mt-8">
              <label class="block uppercase text-gray-700 text-xs font-bold mb-2" for="full-name">Nama Lengkap</label>
              <input type="text"
                class="px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:shadow-outline w-full"
                placeholder="Nama Lengkap" style="transition: all 0.15s ease 0s;" 
                name="full_name"
                value="{{old('full_name')}}"
                />
              @if($errors->has('full_name'))
                  <span class="mt-2 text-sm text-red-500">
                      <strong>{{ $errors->first('full_name') }}</strong>
                  </span>
              @endif
            </div>
            <div class="relative w-full mb-3">
              <label class="block uppercase text-gray-700 text-xs font-bold mb-2" for="email">Email</label><input
                type="email"
                class="px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:shadow-outline w-full"
                placeholder="Email" style="transition: all 0.15s ease 0s;" 
                name="email"
                value="{{old('email')}}"
                />
              
              @if($errors->has('email'))
                  <span class="mt-2 text-sm text-red-500">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif
            </div>
            <div class="relative w-full mb-3">
              <label class="block uppercase text-gray-700 text-xs font-bold mb-2" for="phone">No Telepon/ HP</label><input
                type="text"
                class="px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:shadow-outline w-full"
                placeholder="No Telepon/ HP" style="transition: all 0.15s ease 0s;" 
                name="phone"
                value="{{old('phone')}}"
                />
              @if($errors->has('phone'))
                  <span class="mt-2 text-sm text-red-500">
                      <strong>{{ $errors->first('phone') }}</strong>
                  </span>
              @endif
            </div>
            <div class="text-center mt-6">
              <button
                class="bg-gray-900 text-white active:bg-gray-700 text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1"
                type="submit" style="transition: all 0.15s ease 0s;">
                Daftar Sekarang
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>